export const Galleries = [{
  'galleryId': '1',
  'title': 'Turcja',
  'dateCreated': '2015-12-15T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/turcja1.jpeg',
  'description': 'Kilka dni zwariowanego wypadu do Turcji.',
  'tags': [{
    'tag': 'Turcja'
  },
    {
      'tag': 'Niesamowity ogród w Turcji'
    },
    {
      'tag': 'Niesamowity ogród'
    },
    {
      'tag': 'Ogród'
    },
    {
      'tag': 'Ogrody świata'
    },
    {
      'tag': 'Beautiful'
    }],
  'photos': [{
    'photoId': '1',
    'thumbUrl': './assets/img/gallery/21.jpg',
    'imgUrl': './assets/img/gallery/21.jpg',
  },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/22.jpg',
      'imgUrl': './assets/img/gallery/22.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/23.jpg',
      'imgUrl': './assets/img/gallery/23.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/tur2.jpeg',
      'imgUrl': './assets/img/gallery/tur2.jpeg'
    }]
},{
  'galleryId': '2',
  'title': 'Maroko',
  'dateCreated': '2015-08-07T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/maroko.jpg',
  'description': 'Tydzień zwiedzania południowego Maroka z ojcem.',
  'tags': [{
    'tag': 'Maroko'
  },
    {
      'tag': 'Maroko'
    },
    {
      'tag': 'Agadir'
    },
    {
      'tag': 'Negara'
    },
    {
      'tag': 'Wakacje'
    },
    {
      'tag': 'Beautiful'
    }],
  'photos': [{
    'photoId': '1',
    'thumbUrl': './assets/img/gallery/mar1.jpeg',
    'imgUrl': './assets/img/gallery/mar1.jpeg',
  },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/mar2.jpg',
      'imgUrl': './assets/img/gallery/mar2.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/mar3.jpg',
      'imgUrl': './assets/img/gallery/mar3.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/mar4.jpg',
      'imgUrl': './assets/img/gallery/mar4.jpg'
    }]
}, {
  'galleryId': '3',
  'title': 'Rzym',
  'dateCreated': '2014-11-10T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/rzym.jpg', 'description': 'Dwa tygodnie wycieczki po Rzymu.',
  'tags': [{
    'tag': 'Rzym'
  },
    {
      'tag': 'Rzym'
    },
    {
      'tag': 'Colosseum'
    },
    {
      'tag': 'Wlochy'
    },
    {
      'tag': 'Turystyka'
    },
    {
      'tag': 'Beautiful place'
    },
    {
    'tag': 'Favourite'
    }],
  'photos': [{
    'photoId': '1',
    'thumbUrl': './assets/img/gallery/r1.jpg',
    'imgUrl': './assets/img/gallery/r1.jpg',
  },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/r2.jpg',
      'imgUrl': './assets/img/gallery/r2.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/r3.jpg',
      'imgUrl': './assets/img/gallery/r3.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/r4.jpg',
      'imgUrl': './assets/img/gallery/r4.jpg'
    }]

}, {
  'galleryId': '4',
  'title': 'Chiny',
  'dateCreated': '2013-05-10T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/3.jpg',
  'description': 'Podróże po Chinach.',
  'tags': [{
    'tag': 'Chiny'
  },
    {
      'tag': 'Niesamowity ogród w Chinach'
    },
    {
      'tag': 'Niesamowity ogród'
    },
    {
      'tag': 'Ogród'
    },
    {
      'tag': 'Ogrody świata'
    },
    {
      'tag': 'Piękność'
    }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/img/gallery/51.jpg',
      'imgUrl': './assets/img/gallery/51.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/52.jpg',
      'imgUrl': './assets/img/gallery/52.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/53.jpg',
      'imgUrl': './assets/img/gallery/53.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/ch1.jpg',
      'imgUrl': './assets/img/gallery/ch1.jpg'
    }
  ]
}, {
  'galleryId': '5',
  'title': 'Lisbon',
  'dateCreated': '2017-12-10T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/4.jpg', 'description': 'Spędzamy czas w Portugalii.',
  'tags': [{
    'tag': 'Lisbon'
  },
    {
      'tag': 'Portugal'
    },
    {
      'tag': 'Niesamowity ogród'
    },
    {
      'tag': 'Vasco da gama bridge'
    },
    {
      'tag': 'Favourite'
    },
    {
      'tag': 'Ponte Vasco'
    }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/img/gallery/l1.jpg',
      'imgUrl': './assets/img/gallery/l1.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/l2.jpg',
      'imgUrl': './assets/img/gallery/l2.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/l3.jpg',
      'imgUrl': './assets/img/gallery/l3.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/l4.jpg',
      'imgUrl': './assets/img/gallery/l4.jpg'
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/img/gallery/l5.jpg',
      'imgUrl': './assets/img/gallery/l5.jpg'
    }
  ]
}, {
  'galleryId': '6',
  'title': 'Paryż',
  'dateCreated': '2019-08-10T00:00:00+00:00',
  'thumbUrl': './assets/img/gallery/5.jpg', 'description': 'Pierwszy raz we Francji.',
  'tags': [{
    'tag': 'Paryż'
  },
    {
      'tag': 'France'
    },
    {
      'tag': 'Wieża Eiffla'
    },
    {
      'tag': 'Montparnasse'
    },
    {
      'tag': 'Favourite'
    },
    {
      'tag': 'Plac Trocadero'
    }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/img/gallery/p1.jpg',
      'imgUrl': './assets/img/gallery/p1.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/gallery/p2.jpg',
      'imgUrl': './assets/img/gallery/p2.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/gallery/p3.jpg',
      'imgUrl': './assets/img/gallery/p3.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/gallery/p4.jpg',
      'imgUrl': './assets/img/gallery/p4.jpg'
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/img/gallery/p5.jpg',
      'imgUrl': './assets/img/gallery/p5.jpg'
    }
  ]
}];
