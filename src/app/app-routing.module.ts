import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CommonModule} from "@angular/common";
import {GalleriesComponent} from './components/galleries/galleries/galleries.component';
import {DashboardComponent} from "./components/dashboard/dashboard/dashboard.component";
import {GalleryComponent} from "./components/galleries/gallery/gallery.component";
import {NewsComponent} from "./components/news/news.component";


const routes: Routes = [{
  path: 'galleries',
  component: GalleriesComponent
},{
  path: 'galleries/:galleryId',
  component: GalleryComponent

}, {
  path: 'dashboard',
  component: DashboardComponent
}, {
  path: '',
  redirectTo: '/dashboard',
  pathMatch: 'full'
}, {
  path: 'news',
  component: NewsComponent
}, {
  path: '',
  redirectTo: '/news',
  pathMatch: 'full'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
