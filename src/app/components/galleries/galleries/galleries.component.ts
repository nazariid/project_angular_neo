import { Component, OnInit } from '@angular/core';
import {IGallery} from "../../../interfaces/IGallery";
import {Galleries} from "../../../constants/galleries.constant";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.scss']
})
export class GalleriesComponent implements OnInit {
  title: string;
  description: string;
  galleries: IGallery[];
  searchValue: string;
  limit: number;
  currentPage: number;
  start: number;
  end: number;
  numberOfPages: any = [];
  showGalleryForm: boolean;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '80'
    })
  };
  constructor(private http: HttpClient) {
    this.title = 'Moje podróże';
    this.description = 'Gdzie, kiedy i czemu mnie tam wywiało.';
  }

  ngOnInit() {
    this.setCurrentPage();
    this.galleries = [];
    this.currentPage = parseInt(localStorage.getItem('galleryPage')) || 0;
    this.setCurrentPage(this.currentPage);
    this.showGalleryForm = false;

    this.http.get('http://project.usagi.pl/gallery', this.httpOptions).toPromise().then((response: IGallery[]) => {
      console.log(response);
      this.galleries = response; });
    this.searchValue = '';
    this.numberOfPages = Array(Math.ceil(this.galleries.length /
      this.limit)).fill(1);
  }
  setCurrentPage(page = 0) {
    this.limit = 3;
    this.currentPage = page;
    this.start = this.currentPage * this.limit;
    this.end = this.start + 3;
    localStorage.setItem('galleryPage', this.currentPage.toString());
  }
  setSearchValue($event){
    console.log($event);
    this.searchValue = $event;

  }
  exportGalleries() { Galleries.forEach((gallery: IGallery) => {
    delete(gallery.galleryId);
    this.http.post('http://project.usagi.pl/gallery', gallery, this.httpOptions).toPromise().then((response: IGallery) => {
      console.log('success', response);
      this.galleries.push(response); }, (errResponse) => {
      console.log('error', errResponse); });
    this.numberOfPages = Array(Math.ceil(this.galleries.length /
      this.limit)).fill(1);
  }); }

  removeGalleries() { this.galleries.forEach((gallery: IGallery) => {
    this.http.post('http://project.usagi.pl/gallery/delete/' + gallery.galleryId, {}, this.httpOptions).toPromise().then((response) => {
      this.galleries.splice(0, 1);
      console.log('success', response); }, (errResponse) => {
      console.log('error', errResponse); });
    this.numberOfPages = Array(Math.ceil(this.galleries.length /
      this.limit)).fill(1);
  }); }
  removeGallery(galleryId) {
    const index = this.galleries.findIndex((gallery: IGallery) =>
      gallery.galleryId === galleryId); this.http.post('http://project.usagi.pl/gallery/delete/' + galleryId,
      {}, this.httpOptions).toPromise().then((response) => { this.galleries.splice(index, 1); console.log('success', response);
    }, (errResponse) => { console.log('error', errResponse);
      this.numberOfPages = Array(Math.ceil(this.galleries.length /
        this.limit)).fill(1);
    }); }

  setPrefPage(){
    this.currentPage--;
    this.setCurrentPage(this.currentPage);
  }

  setNextPage(){
    this.currentPage++;
    this.setCurrentPage(this.currentPage);
  }
  saveGallery(event){
    this.http.post('http://project.usagi.pl/gallery', event,
    this.httpOptions).toPromise().then((response) => {
      if (response instanceof IGallery) {
        this.galleries.push(response);
      }
    this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
    this.showGalleryForm = false;
    });
  }


}
