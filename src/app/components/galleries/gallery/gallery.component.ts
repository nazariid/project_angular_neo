import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IGallery} from '../../../interfaces/IGallery';
import {Galleries} from '../../../constants/galleries.constant';
import {IPhoto} from '../../../interfaces/IPhoto';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {IComment} from "../../../interfaces/icomment";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  private galleryId: string;
  private gallery: IGallery;
  private comments: IComment[];
  currentPhoto: IPhoto;
  showGalleryForm: false;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '80'
    }) };
  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.galleryId = this.route.snapshot.paramMap.get('galleryId');
    this.gallery = Galleries.find((item: IGallery) => item.galleryId === this.galleryId);
    this.http.get('http://project.usagi.pl/gallery/' + this.galleryId,
      this.httpOptions).toPromise().then((response: IGallery) => {
      console.log(response);
      this.gallery = response;
      this.currentPhoto = this.gallery.photos[0];
      console.log(this.gallery);
    });
    this.http.get('http://project.usagi.pl/comment/byGallery/' +
      this.galleryId, this.httpOptions).toPromise().then((response: IComment[]) => {
      console.log(response);
      this.comments = response;
    });
  }
  onClick(photo) {
    this.currentPhoto = photo;
  }
  removeComment(commentId){
    const index = this.comments.findIndex(comment => comment.commentId === commentId);
    this.http.post('http://project.usagi.pl/comment/delete/' +
      commentId, {}, this.httpOptions).toPromise().then((response) => {
      this.comments.splice(index, 1);
      console.log('success', response);
    }, (errResponse) => {
      console.log('error', errResponse);
    });
  }
  addComment(comment: IComment){
    this.comments.push(comment);
  }
  saveGallery(event) {
    this.http.post('http://project.usagi.pl/gallery/' + this.galleryId,
      event, this.httpOptions).toPromise().then((response: IGallery) => {
      this.gallery = response;
      this.showGalleryForm = false;
    });
  }
}


