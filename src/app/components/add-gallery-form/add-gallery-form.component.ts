import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import * as uuid from 'uuid/v4';
import {IGallery} from '../../interfaces/IGallery';



@Component({
  selector: 'app-add-gallery-form',
  templateUrl: './add-gallery-form.component.html',
  styleUrls: ['./add-gallery-form.component.scss']
})
export class AddGalleryFormComponent implements OnInit {


  @Input()
  gallery?: IGallery;


  @Output()
  saveGallery: EventEmitter<IGallery> = new EventEmitter<IGallery>();


  constructor() {
  }

  onSubmit() {
    this.saveGallery.emit(this.gallery);
  }

  ngOnInit() {
    this.gallery = (this.gallery && this.gallery.galleryId) ? this.gallery : this.setEmptyGallery();
  }


  setEmptyGallery() {
    return {
      title: '',
      thumbUrl: '',
      dateCreated: '',
      description: '',
      tags: [],
      photos: [
        this.setEmptyPhoto()
      ]
    };
  }

  setEmptyPhoto() {
    return {
      photoId: uuid(),
      thumbUrl: '',
      imgUrl: ''
    };
  }


  removePhoto(index) {
    if (this.gallery.photos.length > 0) {
      this.gallery.photos.splice(index, 1);
    }
  }

  addPhoto() {
    this.gallery.photos.push(this.setEmptyPhoto());
  }
}

