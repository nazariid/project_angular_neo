import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IComment} from '../../../interfaces/icomment';

@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})
export class CommentItemComponent implements OnInit {

  @Input() public comment: IComment;
  @Output() deleteComment: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }
  onDelete(commentId){
    this.deleteComment.emit(commentId);
  }
}
