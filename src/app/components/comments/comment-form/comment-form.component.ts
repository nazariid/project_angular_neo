import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IComment} from "../../../interfaces/icomment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  @Input() galleryId: string;
  @Output() addComment: EventEmitter<IComment> = new EventEmitter<IComment>();
  comment: IComment;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '80'
    })
  };

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.comment = this.setEmptyComment();
    console.log(this.comment);
  }

private setEmptyComment() {
  const newDate = new Date();
  return {
    galleryId: this.galleryId,
    firstName: '',
    lastName: '',
    email: '',
    message: '',
    dateCreated: newDate
  };
  }
  onSubmit(commentForm) {
    console.log('submited!', this.comment);
    this.http.post(`http://project.usagi.pl/comment`, this.comment,
      this.httpOptions).toPromise().then((response: IComment) => {
      console.log(response);
      this.addComment.emit(response);
      this.comment = this.setEmptyComment();
    });
}
}
